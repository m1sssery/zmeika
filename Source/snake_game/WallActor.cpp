// Fill out your copyright notice in the Description page of Project Settings.


#include "WallActor.h"
#include "SnakeBase.h"
#include "D:\Epic Games\games\UE_5.0\Engine\Source\Runtime\CoreUObject\Public\UObject\ConstructorHelpers.h"
// Sets default values
AWallActor::AWallActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


}

// Called when the game starts or when spawned
void AWallActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CollideWall();
}

void AWallActor::CollideWall()
{
	TArray<AActor*>CollectedActors;

	GetOverlappingActors(CollectedActors);

	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		CollectedActors[i]->Destroy(true, true);

	}

}

