// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "snake_gameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_GAME_API Asnake_gameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
