// Copyright Epic Games, Inc. All Rights Reserved.

#include "snake_game.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, snake_game, "snake_game" );
